#!/usr/bin/env bash

VG=""
BUILD="build"
CONFIG="paxos.conf"
OPT="--verbose"

tmux_test ()  {
	tmux new-session -d -s paxos
	tmux new-window -t paxos

	for (( i = 0; i < 6; i++ )); do
		tmux split
		tmux select-layout tiled
	done

	for (( i = 1; i < 4; i++ )); do
		tmux send-keys -t $i "$VG ./$BUILD/sample/acceptor $(( i-1 )) $CONFIG" C-m
	done

	tmux send-keys -t 4 "$VG ./$BUILD/sample/proposer 0 $CONFIG" C-m
	tmux send-keys -t 5 "$VG ./$BUILD/sample/proposer 1 $CONFIG" C-m
	tmux send-keys -t 6 "$VG ./$BUILD/sample/learner $CONFIG" C-m
	tmux send-keys -t 7 "./$BUILD/sample/client $CONFIG"
	tmux selectp -t 7

	tmux attach-session -t paxos
	tmux kill-session -t paxos
}

usage () {
	echo "$0 [--help] [--build-dir dir] [--config-file] [--valgrind]
	[--silence-replica]"
	exit 1
}

while [[ $# > 0 ]]; do
	key="$1"
	case $key in
		-b|--build-dir)
		DIR=$2
		shift
		;;
		-c|--config)
		CONFIG=$2
		shift
		;;
		-h|--help)
		usage
		;;
		-s|--silence-replica)
		OPT=""
		;;
		-v|--valgrind)
		VG="valgrind "
		;;
		*)
		usage
		;;
	esac
	shift
done

tmux_test
